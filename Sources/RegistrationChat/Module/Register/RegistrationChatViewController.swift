//
//  RegistrationChatViewController.swift
//
//
//  Created by Никита Сорокин on 27.01.2023.
//

import Foundation
import UIKit
import CustomElements
import SnapKit

public class RegistrationChatViewController: UIViewController {
    
    fileprivate let nameTextField = PrimaryTextField(initialText: nil, height: 56, tag: 0)
    fileprivate let loginTextField = PrimaryTextField(initialText: nil, height: 56, tag: 1)
    fileprivate let passwordTextField = PrimaryTextField(initialText: nil, height: 56, tag: 2)
    
    fileprivate let regButton = PrimaryButton(title: "Registration", height: 48)
    
    fileprivate lazy var textFieldStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [nameTextField, loginTextField, passwordTextField, regButton])
        nameTextField.primaryDelegate = self
        loginTextField.primaryDelegate = self
        passwordTextField.primaryDelegate = self
        stack.distribution = .fillEqually
        stack.spacing = 16
        stack.alignment = .fill
        stack.axis = .vertical
        stack.setCustomSpacing(32, after: passwordTextField)
        return stack
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTextFieldStack()
    }
    
    private func setupUI() {
        view.backgroundColor = .systemBackground
    }
    
    private func setupTextFieldStack() {
        view.addSubview(textFieldStack)
        textFieldStack.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left).offset(16)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right).offset(-16)
        }
    }
}

extension RegistrationChatViewController: PrimaryTextFieldDelegate {
    public func getValue(withTag tag: Int, value: String?) {
        switch tag {
        case 0: print("Name")
        case 1: print("Login")
        case 2: print("Register")
        default: break
        }
    }
}
